﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ejercicio2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            btnClose.Clicked += async (sender, e) =>
            {
                await Navigation.PopAsync();
            };

        }

        async void OnNextPageButtonClicked(object sender, EventArgs e) //async -> metodo que se ejecutara (en 2do plano) 
        {
            await Navigation.PushAsync(new Page2());
        }
    }
}